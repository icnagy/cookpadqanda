class QuestionsController < ApplicationController
  before_action :authenticate_user!

  def index
    @questions = current_user.questions
  end

  def new
    @question = current_user.questions.build
  end

  def show
    @question = Question.find(params[:id])
  end

  def create
    @question = current_user.questions.create(question_params)
    if @question
      redirect_to question_path(@question)
    else
      render :new
    end
  end

  private

  def question_params
    params.require(:question).permit(:body)
  end
end
