class WelcomeController < ApplicationController
  def index
    @random_quote = Question.order("RANDOM()").first
    @recent_sample = [Question, Answer].map(&:recent).compact
  end
end
