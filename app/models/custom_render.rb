class CustomRender < Redcarpet::Render::HTML
  def image(image_link, title, content)
    %(<image src="#{image_link}" class="img-fluid" alt="#{content}" title="#{title}"/>)
  end
end
