class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question

  validates :body, presence: true

  def self.recent
    order(created_at: :desc).first
  end

  def digest
    body.lines[0..4].join("\r\n")
  end
end
