class User < ApplicationRecord
  devise :database_authenticatable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  has_many :questions, dependent: false
  has_many :answers,   dependent: false

  def self.from_omniauth(access_token)
    data = access_token.info
    user = where(email: data['email']).first

    unless user
        user = create(name: data['name'],
                      email: data['email'],
                      password: Devise.friendly_token[0,20])
    end
    user
  end
end
