require 'redcarpet'
require 'custom_render'

module ApplicationHelper
  def question_owner(question)
    question.user == current_user ? "You" : question.user&.name || "Deleted user"
  end

  def markdown(body)
    renderer = CustomRender.new(filter_html: true, hard_wrap: true)
    @markdown = Redcarpet::Markdown.new( renderer, autolink: true, tables: true)
    @markdown.render(body).html_safe
  end
end
