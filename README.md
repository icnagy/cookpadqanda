# README

## Ruby​ ​on​ ​Rails​ ​Project

We’re​ ​excited​ ​you’ve​ ​applied​ ​to​ ​Cookpad!​ ​We​ ​want​ ​to​ ​make​ ​sure​ ​the​ ​application​ ​process​ ​is​ ​as comfortable​ ​as​ ​possible,​ ​and​ ​for​ ​you​ ​to​ ​feel​ ​like​ ​you’ve​ ​presented​ ​yourself​ ​well.​ ​In​ ​order​ ​to​ ​do this,​ ​we’ve​ ​created​ ​this​ ​simple​ ​project​ ​for​ ​you​ ​to​ ​build​ ​in​ ​Ruby​ ​on​ ​Rails.

We​ ​would​ ​like​ ​you​ ​to​ ​build​ ​a​ ​Question​ ​and​ ​Answer​ ​application​ ​meant​ ​for​ ​internal​ ​use.​ ​The application​ ​doesn’t​ ​need​ ​styling,​ ​but​ ​it​ ​should​ ​be​ ​functional​ ​according​ ​to​ ​the​ ​following specifications:

- Users​ ​can​ ​auth​ ​with​ ​Google​ ​Oauth
- Users​ ​can​ ​submit​ ​questions
- Users​ ​can​ ​submit​ ​answers​ ​to​ ​questions
- Both​ ​answers​ ​and​ ​question​ ​bodies​ ​should​ ​be​ ​in​ ​markdown
- Questions​ ​can​ ​be​ ​searched​ ​by​ ​title,​ ​and​ ​body​ ​(you​ ​can​ ​ignore​ ​answers)

We​ ​would​ ​also​ ​like​ ​for​ ​you​ ​to​ ​submit​ ​a​ ​short​ ​write​ ​up​ ​of​ ​the​ ​project.​ ​Please​ ​let​ ​us​ ​know​ ​if​ ​there​ ​is any​ ​code​ ​that​ ​you’re​ ​particularly​ ​proud​ ​of​ ​in​ ​this​ ​project​ ​as​ ​well​ ​as​ ​what​ ​your​ ​overall​ ​guidelines (design​ ​patterns,​ ​constraints,​ ​etc.)​ ​were​ ​used​ ​for​ ​building​ ​it.​ ​You​ ​are​ ​also​ ​welcome​ ​to​ ​add​ ​any additional​ ​features​ ​you​ ​may​ ​find​ ​interesting,​ ​but​ ​please​ ​document​ ​those​ ​as​ ​well​ ​so​ ​we​ ​make sure​ ​to​ ​look​ ​at​ ​them.
Thank​ ​you​ ​again​ ​for​ ​applying​ ​to​ ​Cookpad.​ ​If​ ​you​ ​have​ ​any​ ​questions​ ​or​ ​comments​ ​about​ ​the project,​ ​please​ ​make​ ​sure​ ​to​ ​ask​ ​as​ ​we​ ​want​ ​your​ ​experience​ ​to​ ​be​ ​as​ ​pleasant​ ​as​ ​possible.​ ​We look​ ​forward​ ​to​ ​reading​ ​your​ ​submission!
