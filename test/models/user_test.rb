require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def test_user_validations
    skip
    invalid_user = User.new
    refute invalid_user.valid?

    assert_includes invalid_user.errors.full_messages.join(','), "Email can't be blank"
    assert_includes invalid_user.errors.full_messages.join(','), "Password can't be blank"
  end
end
