require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  def test_question_must_have_a_user
    invalid_question = Question.new

    refute invalid_question.valid?

    assert_includes invalid_question.errors.full_messages, "User must exist"
  end

  def test_question_cannot_be_empty
    user = users(:one)
    invalid_question = user.questions.new

    refute invalid_question.valid?

    assert_includes invalid_question.errors.full_messages, "Body can't be blank"
  end
end
