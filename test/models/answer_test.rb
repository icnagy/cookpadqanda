require 'test_helper'

class AnswerTest < ActiveSupport::TestCase
  def test_answer_validation
    invalid_answer = Answer.new

    refute invalid_answer.valid?

    assert_includes invalid_answer.errors.full_messages, "User must exist"
    assert_includes invalid_answer.errors.full_messages, "Question must exist"
    assert_includes invalid_answer.errors.full_messages, "Body can't be blank"
  end
end
