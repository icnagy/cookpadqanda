require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def test_index_does_not_authenticate
    get root_url
    assert_response :success
  end
end
