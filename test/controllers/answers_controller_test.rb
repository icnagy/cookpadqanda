require 'test_helper'

class AnswersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def test_create_authenticates
    post question_answers_url(questions(:one)), params: { }
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  def test_create_responds_when_user_is_authenticated
    sign_in users(:one)
    post question_answers_url(questions(:one)), params: { answer: { body: "A simple answer!" } }
    assert_response :redirect
    assert_redirected_to question_url(questions(:one))
    sign_out :user
  end
end
