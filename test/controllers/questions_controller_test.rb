require 'test_helper'

class QuestionsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def test_index_authenticates
    get questions_url
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  def test_show_authenticates
    get question_url(id: questions(:one))
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  def test_create_authenticates
    post questions_url, params: { }
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  def test_index_responds_when_user_is_authenticated
    sign_in users(:one)
    get questions_url
    assert_response :success
    sign_out :user
  end

  def test_show_responds_when_user_is_authenticated
    sign_in users(:one)
    get question_url(questions(:one))
    assert_response :success
    sign_out :user
  end

  def test_create_responds_when_user_is_authenticated
    sign_in users(:one)
    post questions_url, params: { question: { body: "A simple question?" } }
    assert_response :redirect
    assert_redirected_to question_url(Question.last)
    sign_out :user
  end
end
